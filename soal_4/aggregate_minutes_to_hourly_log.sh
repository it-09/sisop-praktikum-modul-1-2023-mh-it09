#--------------------------------------------------------
#!/bin/bash

# Membuat file agregasi
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > metrics_agg_$(date +"%Y%m%d%H").log

# Mengambil data dari file-file menit
for file in metrics_$(date +"%Y%m%d%H")*.log; do
    cat $file >> metrics_agg_$(date +"%Y%m%d%H").log
done

# Melakukan agregasi
awk -F',' 'NR==1{split($0,header)}NR>1{for(i=1;i<=NF;i++)a[i]+=$i}END{for(i=1;i<=NF;i++)print header[i]","a[i]/(NR-1)}' metrics_agg_$(date +"%Y%m%d%H").log >> metrics_agg_$(date +"%Y%m%d%H").log


#--------------------------------------------------------



#crontab
#0 * * * * /home/agas_linux/log/aggregate_minutes_to_hourly_log.sh
