#bin/bash

# Mengambil metrics RAM
metrics=$(free -m)

# Mengambil ukuran direktori
dir_size=$(du -sh /home/agas_linux/log | cut -f1)

# Menyimpan metrics ke dalam file log
timestamp=$(date +"%Y%m%d%H%M%S")
echo "$metrics,/home/agas_linux/log/,$dir_size" >> metrics_$timestamp.log

#------------------------------------------------------------------



#crontab
#* * * * * /home/agas_linux/log/minute_log.sh

