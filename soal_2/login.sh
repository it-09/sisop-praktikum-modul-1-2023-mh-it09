#!/bin/bash

login_success() {
  local username="$1"
  echo "[$(date "+%d/%m/%y %H:%M:%S")] [LOGIN SUCCESS] Welcome, $username"
  echo "[$(date "+%d/%m/%y %H:%M:%S")] [LOGIN SUCCESS] Welcome, $username" >> users/auth.log
}

login_failed() {
  local email="$1"
  local message="$2"
  echo "[$(date "+%d/%m/%y %H:%M:%S")] [LOGIN FAILED] $message for email $email"
  echo "[$(date "+%d/%m/%y %H:%M:%S")] [LOGIN FAILED] $message for email $email" >> users/auth.log
}

echo "----------LOGIN PAGE----------"
read -p "Masukkan email: " email
read -p "Masukkan password: " password

if grep -q "$email" users/users.txt; 
then
  username=$(grep "$email" users/users.txt | awk '{print $2}')
  encrypted_password=$(grep "$email" users/users.txt | awk '{print $3}')
  decrypted_password=$(echo "$encrypted_password" | base64 -d)

if [ "$password" == "$decrypted_password" ]; 
  then
    login_success "$username"
  else
    login_failed "$email" "Incorrect password"
  fi
else
  login_failed "$email" "Email not registered, please register first"
fi

