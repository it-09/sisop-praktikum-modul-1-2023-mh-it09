#!/bin/bash

echo "----------REGISTER PAGE----------"
read -p "Masukkan email: " email
read -p "Masukkan username: " username
read -p "Masukkan password: " password

validasi_pass() {
  local password="$1"
  if [[ ${#password} -ge 8 && "$password" =~ [A-Z] && "$password" =~ [a-z] && "$password" =~ [0-9] && "$password" =~ [\@\#\$\%\^\&\*\(\)\-\_\+] ]];
  then
    echo "valid"
  else
    echo "invalid"
  fi
}

register_success() {
  local username="$1"
  echo "[$(date "+%d/%m/%y %H:%M:%S")] [REGISTER SUCCESS] User $username has been registered successfully."
  echo "[$(date "+%d/%m/%y %H:%M:%S")] [REGISTER SUCCESS] User $username has been registered successfully." >> users/auth.log
}

register_failed() {
  local message="$1"
  echo "[$(date "+%d/%m/%y %H:%M:%S")] [REGISTER FAILED] $message"
  echo "[$(date "+%d/%m/%y %H:%M:%S")] [REGISTER FAILED] $message" >> users/auth.log
}

if grep -q "$email" users/users.txt;
then
  register_failed "The email $email is already registered."
else
  if [ "$password" == "$username" ];
  then
    register_failed "Password cannot be the same as the username."
  else
    if [ "$(validasi_pass "$password")" == "valid" ];
    then
      encrypted_password=$(echo -n "$password" | base64)
      echo "$email $username $encrypted_password" >> users/users.txt
      register_success "$username"
    else
      register_failed "Password does not meet the security requirements."
    fi
  fi
fi
