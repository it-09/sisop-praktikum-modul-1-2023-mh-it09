#!/bin/bash

# 5 Top Lagu dengan Genre Hip Hop
cat playlist.csv | grep "hip hop" | sort -t',' -k15 -n -r | head -n 5

# 5 Lagu Terendah dari John Mayer
cat playlist.csv | grep "John Mayer" | sort -t',' -k15 -n | head -n 5

# 10 Lagu Tahun 2004 dengan Rank Tertinggi
cat playlist.csv | grep ",2004," | sort -t',' -k15 -n -r | head -n 10

# Lagu dengan Pencipta "Sri"
cat playlist.csv | grep "Sri" | head -n 1
