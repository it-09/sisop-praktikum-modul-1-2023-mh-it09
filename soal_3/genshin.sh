#!/bin/bash

# Unzip file genshin.zip ke folder genshin_ekstrak
unzip "genshin.zip" -d "genshin_ekstrak"
cd genshin_ekstrak

# Ada file zip lagi, jadi diunzip dulu dan dimasukkan ke folder genshin_character
unzip "genshin_character.zip" -d "genshin_character"

# Decode nama file yang terenkripsi dengan base64
for file in genshin_character/*.jpg; do
  encoded_name=$(basename "$file" .jpg)
  decoded_name=$(echo "$encoded_name" | base64 -d)

  # Baca data karakter dari list_character.csv
  character_data=$(awk -F"," -v name="$decoded_name" '$1 == name {print}' list_character.csv)

  # Mengganti nama file yang sudah didecode sesuai formatnya
  if [ -n "$character_data" ]; then
     # Pakai sed, karena dipercobaan sebelumnya ada karakter random, untuk menghilangkannya jadi pake sed
    rename=$(echo "$character_data" | awk -F"," '{print $1"-"$2"-"$3"-"$4}' | sed 's/[^A-Za-z0-9._-]//g')
    new_name="$rename.jpg"

    # Ganti nama filenya
    mv "genshin_character/$encoded_name.jpg" "genshin_character/$new_name"

    # Memindahkan file ke folder region sesuai nama regionnya
    region=$(echo "$character_data" | awk -F"," '{print $2}')

    if [ -n "$region" ]; then
      mkdir -p "genshin_character/$region"
      mv "genshin_character/$new_name" "genshin_character/$region/$new_name"
    fi
  fi
done

# Inisialisasi array untuk menghitung pengguna senjata
declare -A senjata_counts

# Loop melalui semua folder genshin_character
for genshin_char in genshin_character/*; do
  if [ -d "$genshin_char" ]; then
    region_name=$(basename "$genshin_char")

    # Loop melalui karakter dalam setiap folder region
    for character_file in "$genshin_char"/*; do
      if [ -f "$character_file" ]; then
        character_name=$(basename "$character_file" .jpg)
        senjata_name=$(echo "$character_name" | awk -F"-" '{print $4}')

        # Menambahkan pengguna senjata ke dalam array
        if [ -n "$senjata_name" ]; then
          ((senjata_counts["$senjata_name"]++))
        fi
      fi
    done
  fi
done


# Menampilkan jumlah pengguna untuk setiap senjata
echo "Jumlah pengguna untuk setiap senjata:"
for senjata in "${!senjata_counts[@]}"; do
  echo "$senjata : ${senjata_counts[$senjata]}"
done

# Menghapus file yang tidak digunakan
rm genshin_character.zip list_character.csv ../genshin.zip

