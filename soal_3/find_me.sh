#!/bin/bash

  # Loop melalui folder genshin_character
  for region in "Fontaine" "Inazuma" "Liyue" "Mondstat" "Sumeru"; do
    folder="./genshin_ekstrak/genshin_character/$region"
    current_time=$(date "+%d/%m/%y %H:%M:%S")
    # Gunakan find untuk mencari file gambar dalam folder region
    find "$folder" -type f -name "*.jpg" -print0 | while read -d $'\0' image; do
      # Ekstrak file menggunakan steghide
      steghide extract -q -p '' -sf "$image" -xf "$folder/decoded.txt"

      # Periksa apakah file .txt berhasil diekstrak
      if [ -e "$folder/decoded.txt" ]; then
        # Dekripsi file .txt menggunakan base64
        base64 -d -i "$folder/decoded.txt" > "$folder/decoded_url.txt"

        # Periksa apakah file .txt mengandung URL yang benar
        url=$(cat "$folder/decoded_url.txt")
        if [[ "$url" == http* ]]; then
          # Download file berdasarkan URL yang didapatkan
          wget "$url" -P ~/soal3_sisop

          # Simpan log
          echo "[$current_time] [FOUND] [$image]" >> ~/soal3_sisop/image.log
        fi

        # Hapus file .txt yang telah didekripsi
        rm "$folder/decoded.txt"
      else
        # Simpan log
        echo "[$current_time] [NOT FOUND] [$image]" >> ~/soal3_sisop/image.log
      fi
    done
  done

