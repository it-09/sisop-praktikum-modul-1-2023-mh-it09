# **LAPORAN RESMI PRAKTIKUM SISTEM OPERASI MODUL 1**

#### **A. SOAL 1**

1. Kita buat folder baru untuk modul 1 nomor 1 : `mkdir modul-1-nomor-1`

2. Download file playlist.csv, bisa menggunakan command wget : `wget --no-check-certificate 'https://drive.google.com/uc?export=download&id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp' -O laguFarel.csv`

3. Buat bash script : `nano playlist_keren.sh`

4. Berikut ini adalah isi dari script `playlist_keren.sh` :

   ![image20](img/Picture25.png)

5. Ini adalah output dari `playlist_keren.sh` setelah dijalankan:

   ![image18](img/Picture26.png)

---

#### **B. SOAL 2**

1. Untuk menyelesaikan soal kedua, diperlukan untuk membuat folder soal_2 menggunakan command `mkdir soal_2`

2. Soal poin A dikerjakan dengan membuat file “register.sh” menggunakan command `nano register.sh`. Isi dari file “register.sh” adalah:

   ![image16](img/Picture27.jpg)

3. Untuk menerima registrasi akun dengan email, username, dan password, maka di dalam file “register.sh” ditulis script berikut:

   ![image10](img/Picture28.jpg)

> `echo` digunakan untuk mencetak header register
> `read -p` digunakan untuk meminta input user menggunakan pesan prompt

4. Soal poin B berisi pengaturan untuk syarat keamanan password yang perlu diinput user. Sehingga, di dalam file ditambahkan fungsi “validasi_pass”.

   ![image39](img/Picture29.jpg)

> `local` digunakan untuk mendeklarasikan variabel lokal, `$1` akan mengambil nilai input pertama saat fungsi dipanggil dan tercatat sebagai password. Dengan menggunakan `echo`, password akan tercetak sebagai “valid” jika memenuhi syarat keamanan, dan “tidak valid” jika tidak memenuhi syarat keamanan.

5. Soal poin C dikerjakan dengan membuat folder baru menggunakan command `mkdir users` lalu `cd users` dan membuat file baru menggunakan command `touch users.txt`. untuk memasukkan data register ke dalam file “users.txt”, digunakan command berikut di dalam file “register.sh”:

   ![image7](img/Picture30.jpg)

- Pada blok command ini, dijalankan command `grep -q “$email” users/users.txt` yang digunakan untuk mencari apakah email yang diinput sudah tercatat/belum di dalam file “users.txt”. Command `echo “$email $username $encrypted_password” >> users/users.txt”` akan mencetak nilai dari input data user ke file “users.txt”. Dalam hal ini, password yang tercatat di dalam file sudah melewati tahap enkripsi menggunakan base64 pada deklarasi variabel `encrypted_password=$(echo -n “$password” | base64)`.

- Blok ini menggunakan if conditional yang memanggil fungsi “register_success”/”register_failed” dengan menerima argumen dan pesan tertentu. Output dari file “users.txt” dapat dilihat menggunakan command `cat users.txt`:

  ![image28](img/Picture31.jpg)

6. Soal poin D dikerjakan dengan mendeklarasikan fungsi baru menggunakan:

   ![image12](img/Picture32.jpg)

- Fungsi “register_success” digunakan untuk mencetak status register dan pesan berhasil. Digunakan command `echo` untuk mencetak tanggal-waktu-status-pesan yang muncul setelah user menginput data registrasi.

- Fungsi “register_failed” digunakan untuk mencetak status register dan pesan gagal. Menggunakan command `echo` dengan tujuan yang sama.

7. File dijalankan dengan menggunakan command `bash register.sh`. Contoh output pesan sukses dan gagal:

   ![image22](img/Picture33.jpg)

8. Soal poin E diselesaikan dengan membuat file baru dengan menggunakan command `touch login.sh`. Isi dari script “login.sh” adalah:

   ![image15](img/Picture34.jpg)

- Login membutuhkan input email dan password dari user menggunakan command:

  ![image29](img/Picture35.jpg)

> `echo` akan mencetak header page dan `read -p` akan meminta input user berupa email dan password.

9. Untuk mengerjakan soal F, maka dideklarasikan fungsi “login_success” dan “login_failed” pada script berikut:

   ![image6](img/Picture36.jpg)

- Fungsi “login_success” memiliki variabel lokal username yang diambil dari argumen pertama setelah fungsi dipanggil dan mencetak tanggal-waktu-status-pesan berhasil menggunakan `echo`.

- Fungsi “login_failed” memiliki variabel lokal email dan massage yang diambil dari 2 argumen saat fungsi dipanggil. Dengan menggunakan `echo`, fungsi ini akan mencetak tanggal-waktu-status-pesan gagal setelah user menginput data.

10. Kedua fungsi diatas dipanggil pada blok command berikut:

    ![image19](img/Picture37.jpg)

- `if grep -q “$email” users/user.txt` digunakan untuk membaca apakah di dalam file “users.txt” terdapat email yang diinput user saat login. Jika ada, akan dideklarasikan variabel username menggunakan command `username=$(grep "$email" users/users.txt | awk '{print $2}')` yang mengambil kolom kedua (username) dari baris email yang ditemukan oleh command `grep`, `awk` digunakan untuk memanipulasi/mengolah data.

- Kemudian, variabel “encrypted_password” akan diambil pada kolom ketiga di baris email tersebut. Setelah “encrypted_password” didapatkan, maka password di decrypted menggunakan command `decrypted_password=$(echo "$encrypted_password" | base64 -d)` yang menggunakan base64 untuk dekripsi.

- Jika “decrypted_password”=password yang diinput user, maka fungsi “login_success” dipanggil dan mencetak “Welcome, $username”, sedangkan jika login gagal (email tidak ditemukan/password salah), maka fungsi “login_failed” akan dipanggil sesuai dengan kondisi kegagalan.

11. File dijalankan dengan menggunakan command `bash login.sh`. Contoh output pesan sukses dan gagal:

    ![image24](img/Picture38.jpg)

12. Untuk menyelesaikan soal poin G, dibuat folder baru di direktori yang sama dengan file login dan register menggunakan command `mkdir users`. Lalu masuk ke direktori users dengan `cd users`, dan membuat file “auth.log” menggunakan command `touch auth.log`. Sehingga akan ada 2 file di dalam folder users (sebelumnya sudah ada users.txt).

    ![image11](img/Picture39.jpg)

13. Berdasarkan penjelasan nomor 6 dan 9, maka di dalam file “auth.log” akan tercatat history tanggal-waktu-status-pesan untuk setiap register success, register failed, login success, dan login failed. Output dari file “auth.log” dapat dilihat menggunakan command `cat auth.log`:

    ![image8](img/Picture40.jpg)

---

#### **C. SOAL 3**

1. Untuk menyelesaikan soal poin A, langkah yang dilakukan pertama adalah mendownload file “genshin.zip” dalam folder task3_sisop mengggunakan command `wget "https://drive.google.com/uc?id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2" -O genshin.zip`

   ![Picture1](img/Picture1.png)

2. Buat file "genshin.sh" dengan command `nano genshin.sh` untuk menjalankan skrip shell agar memenuhi perintah sesuai soal.

3. Tampilan file “genshin.sh”.

   ![Picture2](img/Picture2.png)

4. Ekstrak file "genshin.zip" yang ada dalam direktori yang sama dengan script menggunakan command `unzip "genshin.zip"`

5. Setelah “genshin.zip” diekstrak ternyata ada file “genshin_character.zip” di dalamnya, maka ekstrak file "genshin_character.zip" dan menyimpan hasilnya dalam folder "genshin_character" menggunakan command unzip `"genshin_character.zip" -d "genshin_character"`

6. Di sini mulai menggunakan looping yang akan mengulang setiap file dengan ekstensi ".jpg" di dalam folder "genshin_character." menggunakan command `for file in genshin_character/*.jpg; do` . Looping ini akan mengambil nama berkas (tanpa ekstensi) dan menyimpannya dalam variabel "encoded_name" menggunakan command `encoded_name=$(basename "$file" .jpg)`. Dekode nama file yang sebelumnya dienkripsi dengan base64 dan menyimpan hasilnya dalam variabel "decoded_name" menggunakan command `decoded_name=$(echo "$encoded_name" | base64 -d)`

   ![Picture3](img/Picture3.png)

7. Menggunakan perintah `awk` dan command `character_data=$(awk -F"," -v name="$decoded_name" '$1 == name {print}' list_character.csv)` untuk mencari data karakter yang sesuai dengan nama yang telah didekode dalam file "list_character.csv" dan menyimpannya dalam variabel "character_data".

8. Jika "character_data" tidak kosong, maka akan mengambil data karakter dari CSV dan membuat nama baru yang terdiri dari elemen-elemen karakter yang dipisahkan dengan tanda "-". Kemudian, karakter yang tidak valid (non-alphanumeric, titik, dan garis hubung) dihapus. Kemudian menambahkan ekstensi .jpg ke nama baru yang telah dibuat Menggunakan command berikut:

   ```bash
   if [ -n "$character_data" ]; then
   rename=$(echo "$character_data" | awk -F"," '{print $1"-"$2"-"$3"-"$4}' | sed 's/[^A-Za-z0-9._-]//g')
   new_name="$rename.jpg
   mv "genshin_character/$encoded_name.jpg" "genshin_character/$new_name
   ```

   ![Picture4](img/Picture4.png)

9. Selanjutnya mengambil nama region karakter dari data karakter dengan command `region=$(echo "$character_data" | awk -F"," '{print $2}')`. Jika "region" kosong maka akan membuat direktori region baru menggunakan command `mkdir -p "genshin_character/$region"`. Lalu dipindahkan filenya ke direktori region yang sesuai dengan command `mv "genshin_character/$new_name" "genshin_character/$region/$new_name"`

   ![Picture5](img/Picture5.png)

10. Langkah selanjutnya untuk menyelesaikan soal B dengan mendeklarasikan associative array bernama "weapon_counts" untuk menghitung jumlah pengguna senjata dengan command `declare -A weapon_counts`.

11. Lanjut dengan melakukan looping melalui semua folder region dalam direktori “genshin_character” untuk mencari jumlah pengguna senjata. dengan command `if [ -d "$region_dir" ]; then` , jika `$region_dir` adalah direktori, maka akan mengambil nama region dari direktori dengan command `region_name=$(basename "$region_dir")` . Loop berlanjut untuk mengiterasi melalui karakter dalam setiap folder region.

    ![Picture6](img/Picture6.png)

12. Dengan metode yang mirip dengan sebelumnya, dilakukan looping melalui semua file karakter dalam folder region. Dengan command `if [ -f "$character_file" ]; then` , jika `$character_file` adalah direktori, maka akan mengambil nama karakter dari direktori dengan command `character_name=$(basename "$character_file" .jpg)`.

    ![Picture7](img/Picture7.png)

13. Kemudian dilakukan metode pengambilan nama senjata dari nama karakter dengan command `weapon_name=$(echo "$character_name" | awk -F"-" '{print $3}')`

14. Looping melalui semua file karakter. Dengan command `if [ -n "$weapon_name" ]; then` , jika `$weapon_name` tidak kosong, maka menginkrementasi jumlah pengguna senjata dalam associative array "weapon_counts"

    ![Picture8](img/Picture8.png)

15. Setelah loop-loop selesai, skrip akan menampilkan jumlah pengguna untuk setiap senjata. Sebelumnya akan mencetak pesan awal berupa `echo "Jumlah pengguna untuk setiap senjata:"`. Loop berjalan untuk mengakses setiap senjata dan jumlah pengguna yang terkait. Hasil jumlah pengguna untuk setiap senjata kemudian ditampilkan di terminal dengan command `echo "$weapon : ${weapon_counts[$weapon]}"`

    ![Picture9](img/Picture9.png)

16. Tampilan jumlah pengguna untuk setiap senjata

    ![Picture10](img/Picture10.png)

17. Terakhir dalam sub-soal B, skrip menghapus file “genshin_character.zip” , “list_character.csv” , dan “genshin.zip” dengan command `rm  genshin_character.zip” “list_character.csv” “genshin.zip`

18. Untuk menyelesaikan soal C dan D dibuat script bash yang digunakan untuk melakukan beberapa tugas, seperti mengekstrak data tersembunyi dari file gambar, mengunduh file berdasarkan URL yang ditemukan, dan mencatat log.

19. Pertama, buat file "find_me.sh" dengan command `nano find_me.sh` untuk menjalankan skrip shell agar memenuhi perintah sesuai soal

    ![Picture11](img/Picture11.png)

20. Lalu gunakan looping `for` yang akan mengulang melalui daftar region yang telah ditentukan, yaitu "Fontaine," "Inazuma," "Liyue," "Mondstat," dan "Sumeru" dengan command `for region in "Fontaine" "Inazuma" "Liyue" "Mondstat" "Sumeru"; do`

21. Kemudian buat command `folder="./genshin_character/$region"` akan menginisialisasi variabel "folder" dengan path ke direktori yang sesuai dengan region saat ini.

22. Setelah itu, untuk mengambil waktu saat ini dan menyimpannya dalam variabel "current_time" dalam format yang ditentukan dapat menggunakan command `current_time=$(date "+%d/%m/%y %H:%M:%S")`

    ![Picture12](img/Picture12.png)

23. Menggunakan perintah `find` untuk mencari file gambar dengan ekstensi ".jpg" dalam direktori yang sesuai dengan region saat ini. Lalu dilakukan loop dengan `while` akan membaca daftar file yang ditemukan. Hal itu dilakukan dengan command `find "$folder" -type f -name "*.jpg" -print0 | while read -d $'\0' image; do`

24. Langkah sebelumnya langsung dilanjutkan dengan command `steghide extract -q -p '' -sf "$image" -xf "$folder/decoded.txt"` , menggunakan perintah steghide untuk mencoba mengekstrak data tersembunyi dari file gambar yang ditemukan. Hasil ekstraksi disimpan dalam file "decoded.txt" dalam direktori region saat ini.

    ![Picture13](img/Picture13.png)

25. Dengan command `if [ -e "$folder/decoded.txt" ]; then` , jika “decoded.txt” berhasil diesktrak, maka akan mendekripsi file "decoded.txt" menggunakan base64 dan menyimpan hasilnya dalam file "decoded_url.txt" dalam direktori region saat ini menggunakan command `base64 -d -i "$folder/decoded.txt" > "$folder/decoded_url.txt"`

    ![Picture14](img/Picture14.png)

26. Menggunakan command `url=$(cat "$folder/decoded_url.txt")` untuk membaca isi file "decoded_url.txt" dan menyimpannya dalam variabel url.

27. Command `if [[ "$url" == http* ]]; then` akan dijalankan jika isi `url`dimulai dengan "http" (menandakan bahwa itu adalah URL yang benar).

28. Untuk mengambil nama file gambar (tanpa ekstensi) dan menyimpannya dalam variabel filename digunakan command `filename=$(basename "$image" .jpg)`

29. Jika "decoded.txt" nya benar maka akan direname sesuai nama aslinya menggunakan command `mv "$folder/decoded_url.txt" "$folder/$filename.txt". Lalu dipindahkan ke direktori "~/task3_sisop" dengan command `mv "$folder/$filename.txt" ~/task3_sisop`

30. Pada soal diperintahkan untuk mencatat ke dalam file log bahwa file gambar telah ditemukan dan diunduh. Kemudian digunakanlah command `echo "[$current_time] [FOUND] [$image]" >> ~/task3_sisop/image.log`

    ![Picture15](img/Picture15.png)

31. Kemudian dilanjutkan dengan command `rm "$folder/decoded.txt"` akan menghapus file "decoded.txt" yang telah digunakan, sesuai dengan soal.

32. Jika URL yang ditemukan tidak dimulai dengan "http." maka perintah `echo "[$current_time] [NOT FOUND] [$image]" >> ~/task3_sisop/image.log` digunakan untuk mencatat ke dalam file log bahwa file gambar tidak ditemukan. Kemudian akan menghapus `decode.txt` dengan perintan `rm “$folder/decoded.txt”`

    ![Picture16](img/Picture16.png)

**HASIL**

Pada Soal 3, saya melakukan perbaikan syntax dan code pada "genshin.sh" dan "find_me.sh". Berikut code nya:

```bash
#!genshin.sh

# Unzip file genshin.zip
unzip "genshin.zip"

# Ada file zip lagi, jadi diunzip dulu dan dimasukkan ke folder genshin_character
unzip "genshin_character.zip" -d "genshin_character"

# Decode nama file yang terenkripsi dengan base64
for file in genshin_character/*.jpg; do
  encoded_name=$(basename "$file" .jpg)
  decoded_name=$(echo "$encoded_name" | base64 -d)

  # Baca data karakter dari list_character.csv
  character_data=$(awk -F"," -v name="$decoded_name" '$1 == name {print}' list_character.csv)

  # Mengganti nama file yang sudah didecode sesuai formatnya
  if [ -n "$character_data" ]; then
     # Pakai sed, karena dipercobaan sebelumnya ada karakter random, untuk menghilangkannya jadi pake sed
    rename=$(echo "$character_data" | awk -F"," '{print $1"-"$2"-"$3"-"$4}' | sed 's/[^A-Za-z0-9._-]//g')
    new_name="$rename.jpg"

    # Ganti nama filenya
    mv "genshin_character/$encoded_name.jpg" "genshin_character/$new_name"

    # Memindahkan file ke folder region sesuai nama regionnya
    region=$(echo "$character_data" | awk -F"," '{print $2}')

    if [ -n "$region" ]; then
      mkdir -p "genshin_character/$region"
      mv "genshin_character/$new_name" "genshin_character/$region/$new_name"
    fi
  fi
done

# Inisialisasi associative array untuk menghitung jumlah pengguna senjata
declare -A weapon_counts

# Loop melalui semua folder region
for region_dir in genshin_character/*; do
  if [ -d "$region_dir" ]; then
    region_name=$(basename "$region_dir")

    # Loop melalui karakter dalam setiap folder region
    for character_file in "$region_dir"/*; do
      if [ -f "$character_file" ]; then
        character_name=$(basename "$character_file" .jpg)
        weapon_name=$(echo "$character_name" | awk -F"-" '{print $3}')

        # Menambahkan pengguna senjata ke dalam array
        if [ -n "$weapon_name" ]; then
          ((weapon_counts["$weapon_name"]++))
        fi
      fi
    done
  fi
done

# Menampilkan jumlah pengguna untuk setiap senjata
echo "Jumlah pengguna untuk setiap senjata:"
for weapon in "${!weapon_counts[@]}"; do
  echo "$weapon : ${weapon_counts[$weapon]}"
done

# Menghapus file yang tidak digunakan
rm genshin_character.zip list_character.csv genshin.zip

```

```bash
#!find_me.sh

# Loop melalui folder genshin_character
for region in "Fontaine" "Inazuma" "Liyue" "Mondstat" "Sumeru"; do
  folder="./genshin_character/$region"
  current_time=$(date "+%d/%m/%y %H:%M:%S")

 # Mencari file gambar dalam folder region
  find "$folder" -type f -name "*.jpg" -print0 | while read -d $'\0' image; do
    # Ekstrak file menggunakan steghide
    steghide extract -q -p '' -sf "$image" -xf "$folder/decoded.txt"

    # Periksa apakah file .txt berhasil diekstrak
    if [ -e "$folder/decoded.txt" ]; then
      # Dekripsi file .txt menggunakan base64
      base64 -d -i "$folder/decoded.txt" > "$folder/decoded_url.txt"

      # Periksa apakah file .txt mengandung URL yang benar
      url=$(cat "$folder/decoded_url.txt")
      if [[ "$url" == http* ]]; then
        # Simpan file .txt yang memiliki URL yang benar dengan nama yang sesuai
        filename=$(basename "$image" .jpg)  # Mendapatkan nama file gambar tanpa ekstensi
        if [ -e "$folder/decoded_url.txt" ]; then
          mv "$folder/decoded_url.txt" "$folder/$filename.txt"  # Ubah nama .txt
          mv "$folder/$filename.txt" ~/task3_sisop  # Pindahkan ke folder soal3_sisop
        fi

        # Download file berdasarkan URL yang didapatkan
        wget "$url" -P ~/task3_sisop

        # Simpan log - "FOUND"
        echo "[$current_time] [FOUND] [$image]" >> ~/task3_sisop/image.log
      else
        # Simpan log - "NOT FOUND"
        echo "[$current_time] [NOT FOUND] [$image]" >> ~/task3_sisop/image.log
      fi

      # Hapus file .txt yang telah didekripsi
      rm "$folder/decoded.txt"
    else
      # Simpan log - "NOT FOUND"
      echo "[$current_time] [NOT FOUND] [$image]" >> ~/task3_sisop/image.log
    fi

    # Menambahkan jeda 1 detik
    sleep 1
  done
done

```

1. Output yang diminta soal

   ![Picture17](img/Picture17.png)

2. Cabang/ tree folder

   ![Picture18](img/Picture18.png)
   ![Picture19](img/Picture19.png)

3. Isi file image.log

   ![Picture20](img/Picture20.png)

4. Isi file Lisa-Mondstat-Electro-Catalyst.txt

   ![Picture21](img/Picture21.png)

5. Gambar pada file eab0659ba060b0624df5d28b4910b2fb.jpg

   ![Picture22](img/Picture22.jpg)

**REVISI**

1. Menambahkan syntax atau command untuk memberikan jeda selama 1 detik sebelum melanjutkan ke iterasi berikutnya dengan command `sleep 1`

   ![Picture23](img/Picture23.png)

---

#### **D. SOAL 4**

1. Membuat Script Monitoring Metrics. Buatlah file "minute_log.sh" dengan isi sebagai berikut:

   ![Picture41](img/Picture41.png)

2. Berikan hak akses eksekusi ke file script dengan perintah `chmod +x minute_log.sh`

3. Buat file "aggregate_minutes_to_hourly_log.sh" dengan isi sebagai berikut:

   ![Picture42](img/Picture42.png)

4. Berikan hak akses eksekusi ke file script dengan perintah `chmod +x aggregate_minutes_to_hourly_log.sh`

5. Lakukan `chmod 600 metrics*.log` agar semua file log hanya dapat dibaca oleh pengguna **_agas_linux_**

6. Setelah melakukan run pada file bash "minute_log", didapatkan output yang masih belum sesuai dengan permintaan soal dikarenakan bash script yang kurang sempurna. Berikut ini adalah output dari `bash minute_log.sh`

   ![Picture43](img/Picture43.png)

7. Begitu pula dengan bash script "aggregate_minutes_to_hourly_log.sh" yang kurang sempurna sehingga menghasilkan output sebagai berikut:

   ![Picture44](img/Picture44.png)

8. Selain itu, ketika melakukan `bash aggregate_minutes_to_hourly_log.sh`, terdapat error sebagai berikut:

   ![Picture45](img/Picture45.png)

**REVISI**

1. Dikarenakan output dari "minute_log.sh" dan "aggregate_minutes_to_hourly_log.sh yang belum sesuai", maka dilakukan perubahan pada kedua kode script tersebut.

2. Revisi "minute_log.sh":

   ![Picture47](img/Picture47.png)

3. Output dari revisi "minute_log.sh:

   ![Picture48](img/Picture48.png)

4. Revisi "aggregate_minutes_to_hourly_log.sh":

   ![Picture49](img/Picture49.png)

   ![Picture50](img/Picture50.png)

5. Output dari keseluruhan script aggregate ini masihlah belum sempurna juga, dikarenakan mengalami error lagi.

   ![Picture51](img/Picture51.png)

6. Ini adalah outputnya:

   ![Picture52](img/Picture52.png)
